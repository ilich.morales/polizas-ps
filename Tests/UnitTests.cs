﻿using Application.Common.Dtos.Usuario;
using Application.Common.Interfaces;
using Application.Common.Models;
using Application.Handlers.Usuarios.Commands;
using Application.Handlers.Usuarios.Queries;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    public class UnitTests
    {
        Usuario usr;
        public PolizaDatabaseSettings connection;
        [SetUp]
        public void Setup() {
            connection = new PolizaDatabaseSettings()
            {
                ConnectionString = "mongodb://localhost:27017/?retryWrites=true&w=majority",
                DatabaseName = "test"//default database name from appsettings.json config
            };
            usr = new Usuario()
            {
                Activo = true,
                Email = "test@test.com",
                FechaNacimiento = DateTime.Now,
                Id = "4ff0488c83f73cc3057fa5ed",
                Identificador = "imorales",
                NombreUsuario = "Ilich Daniel Morales",
                Password = "test123",
                Rol = "Admin"
            };
        }

        [Test(Author = "Ilich Daniel Morales", Description = "Validar registro")]
        public async Task Registrar()
        {
            //Arrange
            var imongoDataBase = Substitute.For<IMongoDatabase>();
            var iproperties = Substitute.For<IProperties>();

            iproperties.CollectionUsuario.Returns("usuario");

            //create new database with suffix _unittest
            var database = new MongoClient(connection.ConnectionString).GetDatabase(connection.DatabaseName + "_unittest");
            var collection = database.GetCollection<Usuario>("usuario");
            imongoDataBase.GetCollection<Usuario>("usuario").Returns(collection);

            var registrarCommand = new RegistrarCommand() { 
                Email = "test@mail.com",
                FechaNacimiento = DateTime.UtcNow.AddYears(-20),
                Identificador = "test",
                NombreUsuario = "Usuario de prueba",
                Password = "password123"
            };

            //Act
            RegistrarCommandHandler registrar = new RegistrarCommandHandler(imongoDataBase, iproperties);
            await registrar.Handle(registrarCommand, CancellationToken.None);

            //Assert
            Assert.NotNull(registrar);
            Assert.Pass();
        }

        [Test(Author = "Ilich Daniel Morales", Description = "Validar usuario no existente")]
        [TestCase("NotValid", "xdfNot", false)]
        public async Task Autenticar(string usuario, string password, bool resultExpected)
        {
            //Arrange
            var imongoDataBase = Substitute.For<IMongoDatabase>();
            var iproperties = Substitute.For<IProperties>();

            iproperties.CollectionUsuario.Returns("usuario");
            iproperties.Issuer.Returns("http://dev.mydomain.com.sv");
            iproperties.SignInKey.Returns("88204b45-b6b9-421c-9a08-98476499114d");
            iproperties.ExpiryTimeMinutes.Returns(90);

            var database = new MongoClient(connection.ConnectionString).GetDatabase(connection.DatabaseName);
            var collection = database.GetCollection<Usuario>("usuario");
            imongoDataBase.GetCollection<Usuario>("usuario").Returns(collection);
            
            //Act
            AutenticarCommand registrar = new AutenticarCommand(imongoDataBase, iproperties);
            var result = await registrar.Handle(new AutenticarQuery(usuario, password), CancellationToken.None);

            //Assert
            Assert.AreEqual(resultExpected, result.Succeeded);
            Assert.Pass();
        }

        [Test(Author = "Ilich Daniel Morales", Description = "Validar base de datos y funciones de mongo")]
        [TestCase("imorales", true)]
        [TestCase("noexiste", false)]
        public async Task TestDatabase(string identificador, bool expectedResult)
        {
            //Arrange
            var collection = CreateMockCollection();
            ICollection<Usuario> list = new List<Usuario>() { usr };            
            var imongoDataBase = Substitute.For<IMongoDatabase>();
            imongoDataBase.GetCollection<Usuario>("usuario").Returns(collection);
            var cursorMock = Substitute.For<IAsyncCursor<Usuario>>();
            cursorMock.MoveNextAsync().Returns(Task.FromResult(true), Task.FromResult(false));
            cursorMock.Current.Returns(list);

            var ff = Substitute.For<IFindFluent<Usuario, Usuario>>();
            ff.ToCursorAsync().Returns(Task.FromResult(cursorMock));
            ff.Limit(1).Returns(ff);

            //Act
            var resultado = ff.FirstOrDefaultAsync().Result;

            //Assert
            Assert.AreEqual(expectedResult, resultado.Identificador == identificador);
        }

        public IMongoCollection<Usuario> CreateMockCollection()
        {
            var settings = new MongoCollectionSettings();
            var mockCollection = Substitute.For<IMongoCollection<Usuario>>();// { DefaultValue = DefaultValue.Mock };
            mockCollection.DocumentSerializer.Returns(settings.SerializerRegistry.GetSerializer<Usuario>());
            mockCollection.Settings.Returns(settings);
            return mockCollection;
        }
    }
}
