﻿using Application.Common.Dtos.Poliza;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Application.Handlers.Polizas.Queries
{
    public class GetPolizaByIdQuery : IRequestWrapper<PolizaDto>
    {
        public string Id { get; set; }
    }

    public class GetPolizaByIdHandler : IRequestHandlerWrapper<GetPolizaByIdQuery, PolizaDto>
    {
        private readonly IMongoCollection<Poliza> _polizasCollection;
        private readonly IMapper _map;
        public GetPolizaByIdHandler(IMongoDatabase database, IMapper map, IProperties properties)
        {
            _polizasCollection = database.GetCollection<Poliza>(properties.CollectionPoliza);
            _map = map;
        }
        public async Task<ServiceResult<PolizaDto>> Handle(GetPolizaByIdQuery request, CancellationToken cancellationToken)
        {
            var result = await _polizasCollection.Find(pol => pol.Id == request.Id).SingleOrDefaultAsync(cancellationToken);
            if (result == null)
                throw new KeyNotFoundException($"La póliza con identificador {request.Id} no ha sido encontrada, favor verificar");
            return ServiceResult.Success(result.Adapt<PolizaDto>(_map.Config));
        }
    }
}
