﻿using Application.Common.Dtos.Poliza;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Application.Handlers.Polizas.Queries
{
    public class GetAllPolizasQuery : IRequestWrapper<IList<PolizaDto>>
    {
    }

    public class GetAllPolizasCommand : IRequestHandlerWrapper<GetAllPolizasQuery, IList<PolizaDto>>
    {
        private readonly IMongoCollection<Poliza> _polizasCollection;
        private readonly IMapper _map;
        public GetAllPolizasCommand(IOptions<PolizaDatabaseSettings> settings, IMongoDatabase database, IMapper map, IProperties properties)
        {
            _polizasCollection = database.GetCollection<Poliza>(properties.CollectionPoliza);
            _map = map;
        }
        public async Task<ServiceResult<IList<PolizaDto>>> Handle(GetAllPolizasQuery request, CancellationToken cancellationToken)
        {
            var list = await _polizasCollection.Find(_ => true).ToListAsync(cancellationToken);
            var dto = list.Adapt<IList<PolizaDto>>(_map.Config);
            return ServiceResult.Success(dto);
        }
    }
}
