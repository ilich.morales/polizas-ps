﻿using Application.Common.Dtos.Poliza;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Application.Handlers.Polizas.Commands
{
    public class CreatePolizaCommand : IRequestWrapper<PolizaDto>
    {
        public string CodigoPoliza { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string Nombre { get; set; } = string.Empty;
        public string[] Coberturas { get; set; }
    }

    public class CreatePolizaHandler : IRequestHandlerWrapper<CreatePolizaCommand, PolizaDto>
    {
        private readonly IMongoCollection<Poliza> _polizasCollection;
        private readonly IMapper _map;
        public CreatePolizaHandler(IMongoDatabase database, IMapper map, IProperties properties)
        {
            _polizasCollection = database.GetCollection<Poliza>(properties.CollectionPoliza);
            _map = map;
        }
        public async Task<ServiceResult<PolizaDto>> Handle(CreatePolizaCommand request, CancellationToken cancellationToken)
        {
            try
            {
                Poliza poliza = new Poliza()
                {
                    CodigoPoliza = request.CodigoPoliza,
                    Coberturas = request.Coberturas,
                    FechaFin = request.FechaFin,
                    FechaInicio = request.FechaInicio,
                    Id = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(8),
                    Nombre = request.Nombre
                };
                _polizasCollection.InsertOne(poliza, null, cancellationToken);
                return ServiceResult.Success(poliza.Adapt<PolizaDto>(_map.Config));
            }
            catch (Exception ex)
            {
                return ServiceResult.Failed<PolizaDto>(ServiceError.CustomMessage(ex.Message));
            }
        }
    }
}
