﻿using Application.Common.Dtos.Usuario;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Application.Handlers.Usuarios.Queries
{
    public record AutenticarQuery(string Identificador, string Password) : IRequestWrapper<UsuarioAutenticadoDto> { }
    public class AutenticarCommand : IRequestHandlerWrapper<AutenticarQuery, UsuarioAutenticadoDto>
    {
        private readonly IMongoCollection<Usuario> _usuariosCollection;
        private readonly IProperties props;
        public AutenticarCommand(IMongoDatabase database, IProperties properties)
        {
            _usuariosCollection = database.GetCollection<Usuario>(properties.CollectionUsuario);
            props = properties;
        }
        public async Task<ServiceResult<UsuarioAutenticadoDto>> Handle(AutenticarQuery request, CancellationToken cancellationToken)
        {
            var usr = await _usuariosCollection.
                Find(u => u.Identificador == request.Identificador && u.Password == request.Password && u.Activo == true).
                SingleOrDefaultAsync(cancellationToken);
            // retorna null si el usuario no cumple con las credenciales
            if (usr == null)
                return ServiceResult.Failed<UsuarioAutenticadoDto>(ServiceError.NotFound);

            // Autenticación exitosa, generar JWT Token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(props.SignInKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, usr.NombreUsuario),
                    new Claim(ClaimTypes.Role, usr.Rol)
                }),
                Audience = "mycustomlocalsite.com.sv",
                IssuedAt = DateTime.UtcNow,
                Issuer = props.Issuer,
                Expires = DateTime.UtcNow.AddMinutes(props.ExpiryTimeMinutes),//1 hora default
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Sid, usr.Id));
            if (!string.IsNullOrEmpty(usr.Email))
                tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.Email, usr.Email));
            if (usr.FechaNacimiento.HasValue)
                tokenDescriptor.Subject.AddClaim(new Claim(ClaimTypes.DateOfBirth, usr.FechaNacimiento.Value.ToShortTimeString()));

            var securityToken = tokenHandler.CreateToken(tokenDescriptor);
            string Token = tokenHandler.WriteToken(securityToken);
            var auth = new UsuarioAutenticadoDto()
            {
                Token = Token,
                Expiracion = DateTime.UtcNow.AddMinutes(props.ExpiryTimeMinutes),
                Id = usr.Id,
                Nombre = usr.NombreUsuario
            };
            return ServiceResult.Success(auth);
        }
    }
}
