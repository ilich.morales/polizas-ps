﻿using Application.Common.Interfaces;
using Domain.Entities;
using MediatR;
using MongoDB.Driver;

namespace Application.Handlers.Usuarios.Commands
{
    public class RegistrarCommand : IRequest
    {
        public string Identificador { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string NombreUsuario { get; set; } = string.Empty;
        public string? Email { get; set; }
        public DateTime? FechaNacimiento { get; set; }
    }

    public class RegistrarCommandHandler : IRequestHandler<RegistrarCommand>
    {
        private readonly IMongoCollection<Usuario> _usuariosCollection;
        public RegistrarCommandHandler(IMongoDatabase database, IProperties properties)
        {
            _usuariosCollection = database.GetCollection<Usuario>(properties.CollectionUsuario);
        }
        public async Task Handle(RegistrarCommand request, CancellationToken cancellationToken)
        {
            var exist = await _usuariosCollection.Find(u => u.Identificador == request.Identificador).AnyAsync(cancellationToken);
            if (exist)
                throw new ArgumentException($"El usuario con identificador '{request.Identificador}' ya existe, utilice otro alias.");

            Usuario usr = new Usuario()
            {
                Activo = true,
                Email = request.Email,
                FechaNacimiento = request.FechaNacimiento,
                Id = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(8),
                Identificador = request.Identificador,
                NombreUsuario = request.NombreUsuario,
                Password = request.Password,
                Rol = "Admin"
            };
            await _usuariosCollection.InsertOneAsync(usr, null, cancellationToken);
        }
    }
}
