﻿using Application.Common.Dtos.Poliza;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Mapster;
using MapsterMapper;
using MediatR.Wrappers;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Application.Handlers.SegurosAuto.Queries
{
    public class SeguroAutoPolizaQuery : IRequestWrapper<PolizaDto>
    {
        public string? PlacaVehiculo { get; set; }
        public string? IdPoliza { get; set; }
    }

    public class SeguroAutoPolizaHandler : IRequestHandlerWrapper<SeguroAutoPolizaQuery, PolizaDto>
    {
        private readonly IMongoCollection<SeguroAuto> _seguroCollection;
        private readonly IMongoCollection<Poliza> _polizaCollection;
        private readonly IMapper _map;
        public SeguroAutoPolizaHandler(IOptions<PolizaDatabaseSettings> settings, IMongoDatabase database, IMapper map, IProperties properties)
        {
            _seguroCollection = database.GetCollection<SeguroAuto>(properties.CollectionSeguroAuto);
            _polizaCollection = database.GetCollection<Poliza>(properties.CollectionPoliza);
            _map = map;
        }
        public async Task<ServiceResult<PolizaDto>> Handle(SeguroAutoPolizaQuery request, CancellationToken cancellationToken)
        {
            if (!string.IsNullOrEmpty(request.PlacaVehiculo))
            {
                var result = await _seguroCollection.Find(s => s.PlacaAutomotor == request.PlacaVehiculo).SingleOrDefaultAsync(cancellationToken);
                if (result == null)
                    throw new KeyNotFoundException($"No se ha encontrado vehículo con placa {request.PlacaVehiculo}, favor verificar");
                return ServiceResult.Success(result.Poliza.Adapt<PolizaDto>(_map.Config));
            }
            else if (!string.IsNullOrEmpty(request.IdPoliza))
            {
                var result = await _polizaCollection.Find(s => s.Id == request.IdPoliza).SingleOrDefaultAsync(cancellationToken);
                if (result == null)
                    throw new KeyNotFoundException($"No se ha encontrado la póliza con el identificador {request.IdPoliza}, favor verificar");
                return ServiceResult.Success(result.Adapt<PolizaDto>(_map.Config));
            }
            throw new ArgumentNullException("Debe incluir uno de dos parámetros para la búsqueda 'placaVehiculo' o 'idPoliza'");
        }
    }
}
