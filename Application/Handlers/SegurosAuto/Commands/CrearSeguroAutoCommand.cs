﻿using Application.Common.Dtos.SeguroAuto;
using Application.Common.Interfaces;
using Application.Common.Models;
using Domain.Entities;
using Mapster;
using MapsterMapper;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Application.Handlers.SegurosAuto.Commands
{
    public class CrearSeguroAutoCommand : IRequestWrapper<SeguroAutoDto>
    {
        public string IdPoliza { get; set; }
        public string NombreCliente { get; set; }
        public string IdentificacionCliente { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaPoliza { get; set; }
        public string[] Coberturas { get; set; }
        public decimal ValorMaxCubierto { get; set; }
        public string NombrePlanPoliza { get; set; }
        public string CiudadResidencia { get; set; }
        public string DireccionResidencia { get; set; }
        public string PlacaAutomotor { get; set; }
        public string ModeloAutomotor { get; set; }
        public bool TieneInspeccion { get; set; }
    }

    public class CrearSeguroAutoHandler : IRequestHandlerWrapper<CrearSeguroAutoCommand, SeguroAutoDto>
    {
        private readonly IMongoCollection<SeguroAuto> _seguroCollection;
        private readonly IMongoCollection<Poliza> _polizaCollection;
        private readonly IMapper _map;
        public CrearSeguroAutoHandler(IOptions<PolizaDatabaseSettings> settings, IMongoDatabase database, IMapper map, IProperties properties)
        {
            _seguroCollection = database.GetCollection<SeguroAuto>(properties.CollectionSeguroAuto);
            _polizaCollection = database.GetCollection<Poliza>(properties.CollectionPoliza);
            _map = map;
        }
        public async Task<ServiceResult<SeguroAutoDto>> Handle(CrearSeguroAutoCommand request, CancellationToken cancellationToken)
        {
            var poliza = await _polizaCollection.Find(p => p.Id == request.IdPoliza).SingleOrDefaultAsync();
            if (poliza == null)
                throw new KeyNotFoundException($"El identificador de póliza con id '{request.IdPoliza}' no es válido");
            if (request.FechaPoliza < poliza.FechaInicio || request.FechaPoliza > poliza.FechaFin)
                throw new HttpRequestException($"La vigencia de la póliza {poliza.Nombre} es entre {poliza.FechaInicio.ToShortDateString()} y {poliza.FechaFin.ToShortDateString()}, y " +
                    $"está intentando guardar con fecha {request.FechaPoliza.ToShortDateString()}, favor verifique");
            var seguroAuto = new SeguroAuto()
            {
                ModeloAutomotor = request.ModeloAutomotor,
                PlacaAutomotor = request.PlacaAutomotor,
                CiudadResidencia = request.CiudadResidencia,
                Coberturas = request.Coberturas,
                DireccionResidencia = request.DireccionResidencia,
                FechaNacimiento = request.FechaNacimiento,
                FechaPoliza = request.FechaPoliza,
                Id = Guid.NewGuid().ToString().Replace("-", string.Empty).Substring(8),
                IdentificacionCliente = request.IdentificacionCliente,
                NombreCliente = request.NombreCliente,
                NombrePlanPoliza = request.NombrePlanPoliza,
                Poliza = poliza,
                TieneInspeccion = request.TieneInspeccion,
                ValorMaxCubierto = request.ValorMaxCubierto
            };
            await _seguroCollection.InsertOneAsync(seguroAuto, null, cancellationToken);
            return ServiceResult.Success(seguroAuto.Adapt<SeguroAutoDto>(_map.Config));
        }
    }
}
