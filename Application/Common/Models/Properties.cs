﻿using Application.Common.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Common.Models
{
    public class Properties : IProperties
    {
        private string _signInKey;
        private string _issuer;
        private int _expiryTimeMinutes;
        private string _collectionPoliza;
        private string _collectionSeguroAuto;
        private string _collectionUsuario;
        public Properties(IConfiguration conf)
        {
            _signInKey = conf["JwtKeys:SignInKey"];
            _issuer = conf["JwtKeys:Issuer"];
            _expiryTimeMinutes = Convert.ToInt32(conf["JwtKeys:ExpiryTimeMinutes"]);
            _collectionPoliza = conf["Collections:Poliza"];
            _collectionSeguroAuto = conf["Collections:SeguroAuto"];
            _collectionUsuario = conf["Collections:Usuario"];
        }
        public string SignInKey => _signInKey;
        public string Issuer => _issuer;
        public int ExpiryTimeMinutes => _expiryTimeMinutes;
        public string CollectionPoliza => _collectionPoliza;
        public string CollectionUsuario => _collectionUsuario;
        public string CollectionSeguroAuto => _collectionSeguroAuto;
    }
}
