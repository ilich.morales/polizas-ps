﻿
using Application.Common.Interfaces;

namespace Application.Common.Models
{
    public class PolizaDatabaseSettings
    {   
        public string ConnectionString { get; set; } = null!;
        public string DatabaseName { get; set; } = null!;
    }
}
