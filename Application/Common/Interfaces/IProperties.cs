﻿namespace Application.Common.Interfaces
{
    public interface IProperties
    {
        string SignInKey { get; }
        string Issuer { get; }
        int ExpiryTimeMinutes { get; }

        string CollectionPoliza { get; }
        string CollectionUsuario { get; }
        string CollectionSeguroAuto { get; }
    }
}
