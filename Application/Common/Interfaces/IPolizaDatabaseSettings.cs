﻿namespace Application.Common.Interfaces
{
    public interface IPolizaDatabaseSettings
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
