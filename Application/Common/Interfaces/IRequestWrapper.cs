﻿using Application.Common.Models;
using MediatR;

namespace Application.Common.Interfaces
{
    public interface IRequestWrapper<T> : IRequest<ServiceResult<T>>
    {
    }

    public interface IRequestHandlerWrapper<Tin, Tout> : IRequestHandler<Tin, ServiceResult<Tout>> where Tin : IRequestWrapper<Tout>
    {

    }
}
