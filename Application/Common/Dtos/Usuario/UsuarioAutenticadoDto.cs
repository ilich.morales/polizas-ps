﻿namespace Application.Common.Dtos.Usuario
{
    public class UsuarioAutenticadoDto
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Token { get; set; }
        public DateTime Expiracion { get; set; }
    }
}
