﻿using Application.Common.Dtos.Poliza;
using Domain.Entities;
using Mapster;

namespace Application.Common.Dtos.SeguroAuto
{
    public class SeguroAutoDto : IRegister
    {
        public string Id { get; set; }
        public PolizaDto Poliza { get; set; }
        public string NombreCliente { get; set; }
        public string IdentificacionCliente { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public DateTime FechaPoliza { get; set; }
        public string[] Coberturas { get; set; }
        public decimal ValorMaxCubierto { get; set; }
        public string NombrePlanPoliza { get; set; }
        public string CiudadResidencia { get; set; }
        public string DireccionResidencia { get; set; }
        public string PlacaAutomotor { get; set; }
        public string ModeloAutomotor { get; set; }
        public bool TieneInspeccion { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<Domain.Entities.SeguroAuto, SeguroAutoDto>().TwoWays();
        }
    }
}
