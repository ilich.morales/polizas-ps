﻿using Domain.Entities;
using Mapster;

namespace Application.Common.Dtos.Poliza
{
    public class PolizaDto : IRegister
    {
        public string? Id { get; set; }
        public string CodigoPoliza { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public string Nombre { get; set; }
        public string[] Coberturas { get; set; }

        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<PolizaDto, Domain.Entities.Poliza>().TwoWays();
        }
    }
}
