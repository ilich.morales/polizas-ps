﻿using Application.Common.Interfaces;
using Application.Common.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
//using System.Configuration;
//using Microsoft.AspNetCore.Builder;

namespace Infrastructure
{
    public static class InfrastructureDependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, ConfigurationManager configuration)
        {
            PolizaDatabaseSettings connection = new PolizaDatabaseSettings();
            configuration.Bind("Database", connection);
            services.AddSingleton(d => new MongoClient(connection.ConnectionString).GetDatabase(connection.DatabaseName));
            return services;
        }
    }
}