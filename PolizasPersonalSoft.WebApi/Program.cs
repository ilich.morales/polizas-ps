using Application;
using Application.Common.Interfaces;
using Application.Common.Models;
using FluentValidation.AspNetCore;
using Infrastructure;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using PolizasPersonalSoft.WebApi.Helpers;
using System;
using System.Configuration;
using System.Text;

namespace PolizasPersonalSoft.WebApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddApplication();

            builder.Services.Configure<PolizaDatabaseSettings>(builder.Configuration.GetSection("Database"));
            builder.Services.AddInfrastructure(builder.Configuration);

            builder.Services.AddControllers();
            builder.Services.AddFluentValidationAutoValidation();//x => x.DisableDataAnnotationsValidation = false
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            OpenApiSecurityScheme securityScheme = new OpenApiSecurityScheme()
            {
                Description = "Ingrese token de seguridad en campo pendiente.\r\n\r\nEjemplo: 'Bearer ey12345abcdef'",
                Name = "Authorization",
                In = ParameterLocation.Header,
                //BearerFormat = "JWT",
                Type = SecuritySchemeType.ApiKey,
                Scheme = JwtBearerDefaults.AuthenticationScheme,
                Reference = new OpenApiReference()
                {
                    Id = JwtBearerDefaults.AuthenticationScheme,
                    Type = ReferenceType.SecurityScheme
                }
            };
            IProperties properties = new Properties(builder.Configuration);
            OpenApiSecurityRequirement securityRequirement = new OpenApiSecurityRequirement();
            securityRequirement.Add(securityScheme, new List<string>());
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "Polizas Personal Soft Web API",
                    Description = "Web API RestFul para pruebas de PersonalSoft",
                    Contact = new OpenApiContact() { Name = "Ilich Morales", Email = "ilich.daniel.morales@gmail.com" }
                });
                c.SchemaFilter<EnumSchemaFilter>();
                c.AddSecurityRequirement(securityRequirement);
                c.AddSecurityDefinition(JwtBearerDefaults.AuthenticationScheme, securityScheme);
            });
            builder.Services.AddApiVersioning(
            opt =>
            {
                opt.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                opt.AssumeDefaultVersionWhenUnspecified = true;
                opt.ReportApiVersions = true;
                opt.ApiVersionReader = ApiVersionReader.Combine(new UrlSegmentApiVersionReader(),
                                                                new HeaderApiVersionReader("x-api-version"),
                                                                new MediaTypeApiVersionReader("x-api-version"));
            });
            string ServerAddress = properties.Issuer;
            AuthValidations authValidation = new AuthValidations(properties);
            var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(properties.SignInKey));
            var validationParameters = new TokenValidationParameters
            {
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidIssuer = ServerAddress,
                ValidateIssuerSigningKey = false,
                AudienceValidator = authValidation.AudienceValidation,
                IssuerValidator = authValidation.IssuerValidation,
                IssuerSigningKey = key,
                RequireSignedTokens = true,
                RoleClaimTypeRetriever = authValidation.RoleClaimTypeRetreiverAssign,
                RoleClaimType = "string"
            };

            builder.Services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(opts =>
            {
                opts.TokenValidationParameters = validationParameters;
                opts.RequireHttpsMetadata = false;
                opts.SaveToken = true;
                opts.Validate();
            });
            builder.Services.AddAuthorization();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint($"/swagger/v1/swagger.json", "Polizas Personal Soft Web API");
                    c.DefaultModelExpandDepth(-1);
                    c.DisplayRequestDuration();
                });
            }
            app.UseAuthentication();

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseHttpsRedirection();
            app.UseAuthorization();
            app.MapControllers();

            app.Run();
        }
    }
}