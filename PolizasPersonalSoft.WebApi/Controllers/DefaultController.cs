﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace PolizasPersonalSoft.WebApi.Controllers
{
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiVersion("1.0")]
    [ApiController]
    public class DefaultController : ControllerBase
    {
        private ISender _mediatr;
        protected ISender Mediatr => _mediatr != null ? _mediatr : HttpContext.RequestServices.GetService<ISender>();
    }
}
