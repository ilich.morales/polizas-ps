﻿using Application.Common.Dtos.Poliza;
using Application.Common.Models;
using Application.Handlers.Polizas.Commands;
using Application.Handlers.Polizas.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PolizasPersonalSoft.WebApi.Controllers
{
    [Authorize]
    public class PolizaController : DefaultController
    {
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceResult<PolizaDto>))]
        public async Task<IActionResult> Post([FromBody] CreatePolizaCommand create, CancellationToken cancellationToken)
        {
            var result = await Mediatr.Send(create, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status500InternalServerError, result);
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceResult<PolizaDto>))]
        public async Task<IActionResult> Get(CancellationToken cancellationToken)
        {
            var result = await Mediatr.Send(new GetAllPolizasQuery(), cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status500InternalServerError, result);
        }

        [HttpGet("{idPoliza}")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ServiceResult<PolizaDto>))]
        public async Task<IActionResult> GetById([FromRoute] string idPoliza, CancellationToken cancellationToken)
        {
            var result = await Mediatr.Send(new GetPolizaByIdQuery() { Id = idPoliza }, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status500InternalServerError, result);
        }
    }
}
