﻿using Application.Handlers.Usuarios.Commands;
using Application.Handlers.Usuarios.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using System.Text;

namespace PolizasPersonalSoft.WebApi.Controllers
{
    [AllowAnonymous]
    public class UsuarioController : DefaultController
    {
        [HttpPost("Autenticar")]        
        public async Task<IActionResult> Autenticar([FromBody] AutenticarQuery request, CancellationToken cancellationToken)
        {
            var result = await Mediatr.Send(request, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status400BadRequest);
        }

        [HttpPost("Registrar")]
        public async Task<IActionResult> Registrar([FromBody] RegistrarCommand request, CancellationToken cancellationToken)
        {
            await Mediatr.Send(request, cancellationToken);
            return NoContent();
        }
    }
}
