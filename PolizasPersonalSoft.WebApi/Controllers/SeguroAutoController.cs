﻿using Application.Handlers.SegurosAuto.Commands;
using Application.Handlers.SegurosAuto.Queries;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace PolizasPersonalSoft.WebApi.Controllers
{
    [Authorize]
    public class SeguroAutoController : DefaultController
    {
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CrearSeguroAutoCommand request, CancellationToken cancellationToken)
        {
            var result = await Mediatr.Send(request, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status500InternalServerError);
        }

        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string? placaVehiculo, [FromQuery] string? idPoliza, CancellationToken cancellationToken)
        {
            SeguroAutoPolizaQuery request = new SeguroAutoPolizaQuery()
            {
                IdPoliza = idPoliza,
                PlacaVehiculo = placaVehiculo
            };
            var result = await Mediatr.Send(request, cancellationToken);
            if (result.Succeeded)
                return Ok(result);
            return StatusCode(StatusCodes.Status500InternalServerError);
        }
    }
}
