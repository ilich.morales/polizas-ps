﻿using Application.Common.Interfaces;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace PolizasPersonalSoft.WebApi.Helpers
{
    public class AuthValidations
    {
        private readonly IProperties props;
        public AuthValidations(IProperties properties)
        {
            props = properties ?? throw new ArgumentNullException(nameof(properties));
        }

        public bool AudienceValidation(IEnumerable<string> audiences, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            var castedToken = securityToken as JwtSecurityToken;
            return audiences.FirstOrDefault() == castedToken.Audiences.FirstOrDefault();
        }

        public bool SignInKeyValidation(SecurityKey securityKey, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            var castedToken = securityToken as JwtSecurityToken;
            return validationParameters.IssuerSigningKey == securityKey && validationParameters.IssuerSigningKey == castedToken.SigningKey;
        }

        public string IssuerValidation(string issuer, SecurityToken securityToken, TokenValidationParameters validationParameters)
        {
            var castedToken = securityToken as JwtSecurityToken;
            if (issuer == $"{props.Issuer}" && issuer == validationParameters.ValidIssuer &&
                issuer == castedToken.Issuer)
                return issuer;
            return null;
        }

        public string RoleClaimTypeRetreiverAssign(SecurityToken securityToken, string roleType)
        {
            var castedToken = securityToken as JwtSecurityToken;
            var roleClaim = castedToken.Claims.SingleOrDefault(s => s.Type == "role");
            return roleClaim.Value;
        }
    }
}
