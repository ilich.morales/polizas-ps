﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Domain.Entities
{
    public class Usuario
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("identificador")]
        public string Identificador { get; set; }
        [BsonElement("password")]
        public string Password { get; set; }
        [BsonElement("activo")]
        public bool Activo { get; set; }
        [BsonElement("nombreUsuario")]
        public string NombreUsuario { get; set; }
        [BsonElement("rol")]
        public string Rol { get; set; }
        [BsonElement("email")]
        public string? Email { get; set; }
        [BsonElement("fechaNacimiento")]
        public DateTime? FechaNacimiento { get; set; }
    }
}
