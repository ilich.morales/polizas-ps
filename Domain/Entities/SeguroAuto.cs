﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace Domain.Entities
{
    public class SeguroAuto
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("poliza")]
        public Poliza Poliza { get; set; }
        [BsonElement("nombreCliente")]
        public string NombreCliente { get; set; }
        [BsonElement("identificacionCliente")]
        public string IdentificacionCliente { get; set; }
        [BsonElement("fechaNacimiento")]
        public DateTime FechaNacimiento { get; set; }
        [BsonElement("fechaPoliza")]
        public DateTime FechaPoliza { get; set; }
        [BsonElement("coberturas")]
        public string[] Coberturas { get; set; }
        [BsonElement("valorMaxCubierto")]
        public decimal ValorMaxCubierto { get; set; }
        [BsonElement("nombrePlanPoliza")]
        public string NombrePlanPoliza { get; set; }
        [BsonElement("ciudadResidencia")]
        public string CiudadResidencia { get; set; }
        [BsonElement("direccionResidencia")]
        public string DireccionResidencia { get; set; }
        [BsonElement("placaAutomotor")]
        public string PlacaAutomotor { get; set; }
        [BsonElement("modeloAutomotor")]
        public string ModeloAutomotor { get; set; }
        [BsonElement("tieneInspeccion")]
        public bool TieneInspeccion { get; set; }
    }
}
