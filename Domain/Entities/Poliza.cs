﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Domain.Entities
{
    public class Poliza
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("codigoPoliza")]
        public string CodigoPoliza { get; set; }
        [BsonElement("fechaInicio")]
        public DateTime FechaInicio { get; set; }
        [BsonElement("fechaFin")]
        public DateTime FechaFin { get; set; }
        [BsonElement("nombre")]
        public string Nombre { get; set; }
        [BsonElement("coberturas")]
        public string[] Coberturas { get; set; }
    }
}
